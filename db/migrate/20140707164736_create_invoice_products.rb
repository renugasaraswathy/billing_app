class CreateInvoiceProducts < ActiveRecord::Migration
  def change
    create_table :invoice_products do |t|
      t.integer :invoice_id
      t.string :product_id
      t.integer :quantity

      t.timestamps
    end
  end
end
