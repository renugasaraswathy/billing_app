class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :no
      t.datetime :date

      t.timestamps
    end
  end
end
