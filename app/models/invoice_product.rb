class InvoiceProduct < ActiveRecord::Base
  attr_accessible :invoice_id, :product_id, :quantity
  belongs_to :invoice
  belongs_to :product  
end
