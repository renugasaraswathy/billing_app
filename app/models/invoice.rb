class Invoice < ActiveRecord::Base
  attr_accessible :date, :no,:invoice_products_attributes
  has_many :products,:through=>:invoice_products
  has_many :invoice_products
  accepts_nested_attributes_for :invoice_products
end
